FROM maven:3-jdk-8-alpine as builder

WORKDIR /app
COPY pom.xml pom.xml
RUN mvn dependency:go-offline

COPY src/ src/
RUN mvn clean package -DfinalName=app


FROM openjdk:8-alpine
COPY --from=builder /app/target/app.jar /app.jar
CMD  [ "java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar" ]
