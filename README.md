# Glossy Basket

## Overview

The Glossy Basket is an API application that allows users to query data on Places-of-Interest (POI) using a variety of options as well as to manage them.

## Developer Onboarding

### Prerequisites

#### Design

- [Swagger/OpenAPI Editor](https://editor.swagger.io/)

#### Development

- [Git](https://git-scm.com/download)
- [OpenJDK 8](https://openjdk.java.net/install)
- [Maven 3](https://maven.apache.org/download.cgi) (_Not required if the [Maven Wrapper](https://github.com/takari/maven-wrapper) is to be used_)
- [Docker CE](https://docs.docker.com/get-docker)
- Your preferred [IDE](https://en.wikipedia.org/wiki/Integrated_development_environment) (_e.g._ [Eclipse](https://www.eclipse.org/downloads), [IntelliJ IDEA](https://www.jetbrains.com/idea/download), [VS Code](https://code.visualstudio.com/download))

#### Testing

Any of the following tools for calling API endpoints:
- [`curl`](https://curl.haxx.se/download.html)
- [Postman](https://www.getpostman.com/)

### Libraries used

- [Spring Boot 2.4](https://spring.io/projects/spring-boot)
- [JUnit 5](https://junit.org/junit5)
- [H2 Database](https://www.h2database.com/html/main.html)

### Development Environment Setup

1. Checkout the code:
```sh
$ git clone https://gitlab.com/napramirez/glossybasket.git
```
2. Change working directory:
```sh
$ cd glossybasket
```
3. Install dependencies:
```sh
$ mvn dependency:go-offline
```
4. Import the directory/Maven project in your preferred IDE

### Building the application package

1. [`JAR`](https://en.wikipedia.org/wiki/JAR_(file_format)) file
```sh
$ mvn clean package
```
> NOTE: The `JAR` file is located at `<project-root-directory>/target/glossybasket-0.0.1-SNAPSHOT.jar`

2. Docker image
```sh
$ docker build -t napramirez/glossybasket .
```
> NOTE: The Docker build wraps the Maven build (including the running of built-in tests).

### Running the application

1. Working or development environment
```sh
$ mvn spring-boot:run
```
> NOTE: Press `Ctrl+C` to terminate the running application.

2. Running the [`JAR`](https://en.wikipedia.org/wiki/JAR_(file_format)) file:
```sh
$ java -jar `<project-root-directory>/target/glossybasket-0.0.1-SNAPSHOT.jar`
```
> NOTE: Press `Ctrl+C` to terminate the running application.

3. Running the Docker image
```sh
$ docker run -ti --rm -p 8080:8080 napramirez/glossybasket
```
> NOTE: Press `Ctrl+C` to terminate the running application.

### Running the built-in tests

1. Working or development environment
```sh
$ mvn clean test
```

2. Manual test (using `curl`)

```sh
$ curl -sX GET http://localhost:8080/poi/groups?location=Victoria%20St
$ curl -sX POST -H 'Content-Type: application/json' http://localhost:8080/poi/search -d '{"geoPoint":{"lat":1.29684825487647,"long":103.85253591654006}}'
$ curl -sX GET http://localhost:8080/poi/search?name=Blk%20461
$ curl -sX PATCH -H 'Content-Type: application/json' http://localhost:8080/poi/100 -d '{"name":"Aft Chijmesss"}'
```

## Developer Practices

1. [Test Driven Development](https://martinfowler.com/bliki/TestDrivenDevelopment.html)
2. [Emergent Design](https://en.wikipedia.org/wiki/Emergent_Design#Emergent_design_in_agile_software_development)
3. Design with interfaces (_Related_: [API-First Approach](https://swagger.io/resources/articles/adopting-an-api-first-approach/))
4. Adopt a low complexity design and keep close to business concepts
5. Make the code readable, so formal documentation can be reduced
6. _"Commit early, commit often"_
