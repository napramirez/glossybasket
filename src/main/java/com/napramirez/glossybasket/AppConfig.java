package com.napramirez.glossybasket;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    
    @Value("${DEFAULT_SEARCH_RADIUS:10}")
    private Double defaultSearchRadius;

    public Double getDefaultSearchRadius() {
        return defaultSearchRadius;
    }
}
