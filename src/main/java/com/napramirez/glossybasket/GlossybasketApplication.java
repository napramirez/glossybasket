package com.napramirez.glossybasket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GlossybasketApplication {

	public static void main(String[] args) {
		SpringApplication.run(GlossybasketApplication.class, args);
	}

}
