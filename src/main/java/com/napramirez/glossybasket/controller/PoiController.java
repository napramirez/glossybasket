package com.napramirez.glossybasket.controller;

import com.napramirez.glossybasket.dto.GeoPoint;
import com.napramirez.glossybasket.dto.UpdatePoiRequest;
import com.napramirez.glossybasket.service.PoiService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PoiController {

    private PoiService poiService;

    public PoiController(PoiService poiService) {
        this.poiService = poiService;
    }
    
    @PatchMapping(path = "/poi/{poiId}")
    public ResponseEntity<?> updatePoi(
            @PathVariable("poiId") Integer poiId,
            @RequestBody UpdatePoiRequest request) {

        if (request == null) {
            return ResponseEntity.badRequest().build();
        }

        String name = request.getName();
        String location = request.getLocation();
        GeoPoint geoPoint = request.getGeoPoint();
    
        if (name == null && location == null && geoPoint == null) {
            return ResponseEntity.badRequest().build();
        }

        if (name != null && name.trim().isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        if (location != null && location.trim().isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        if (geoPoint != null) {
            Double latitude = geoPoint.getLatitude();
            Double longitude = geoPoint.getLongitude();
            if (latitude == null || longitude == null) {
                return ResponseEntity.badRequest().build();
            }
            
            if (latitude < -90 || latitude > 90) {
                return ResponseEntity.badRequest().build();
            }

            if (longitude < -180 || longitude > 180) {
                return ResponseEntity.badRequest().build();
            }
        }

        if (poiService.updatePoi(poiId, request) == 0) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.noContent().build();
    }
}
