package com.napramirez.glossybasket.controller;

import java.util.List;

import com.napramirez.glossybasket.dto.GroupedPoisResult;
import com.napramirez.glossybasket.service.PoiGroupService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PoiGroupController {

    private PoiGroupService poiGroupService;

    public PoiGroupController(PoiGroupService poiGroupService) {
        this.poiGroupService = poiGroupService;
    }
    
    @GetMapping(path = "/poi/groups")
    public ResponseEntity<?> poiGroups(@RequestParam(name = "location", required = false) List<String> locations) {
        
        GroupedPoisResult body = new GroupedPoisResult();

        if (locations != null) {
            for (String location : locations) {
                body.put(location, poiGroupService.getGroupedPois(location));
            }
        }

        return ResponseEntity.ok().body(body);
    }

}
