package com.napramirez.glossybasket.controller;

import com.napramirez.glossybasket.dto.SearchPoisMatchSummaryResult;
import com.napramirez.glossybasket.dto.SearchPoisRequest;
import com.napramirez.glossybasket.dto.SearchPoisResult;
import com.napramirez.glossybasket.service.PoiSearchService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PoiSearchController {

    @Autowired
    private PoiSearchService poiSearchService;

    public PoiSearchController(PoiSearchService poiSearchService) {
        this.poiSearchService = poiSearchService;
    }
    
    @PostMapping(path = "/poi/search")
    public ResponseEntity<?> searchPois(
            @RequestHeader(name = HttpHeaders.CONTENT_TYPE) String contentType,
            @RequestBody SearchPoisRequest request) {

        if (request == null || request.getGeoPoint() == null) {
            return ResponseEntity.badRequest().build();
        }

        Double latitude = request.getGeoPoint().getLatitude();
        if (latitude == null || latitude < -90 || latitude > 90) {
            return ResponseEntity.badRequest().build();
        }

        Double longitude = request.getGeoPoint().getLongitude();
        if (longitude == null || longitude < -180 || longitude > 180) {
            return ResponseEntity.badRequest().build();
        }

        SearchPoisResult result = poiSearchService.searchNearbyPois(request);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(result);
    }

    @GetMapping(path = "/poi/search")
    public ResponseEntity<?> searchPois(@RequestParam(name = "name", required = false) String name) {

        if (name == null || name.trim().isEmpty()) {
            SearchPoisMatchSummaryResult result = new SearchPoisMatchSummaryResult();
            result.setMatchCount(0);
            result.setExactMatchCount(0);
    
            return ResponseEntity.ok().body(result);
        }

        SearchPoisMatchSummaryResult result = poiSearchService.searchByName(name);

        return ResponseEntity.ok().body(result);
    }

}
