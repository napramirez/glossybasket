package com.napramirez.glossybasket.dto;

import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class GroupedPoisResult extends HashMap<String, List<PoiRecordWithStringifiedGeoPoint>> {

	private static final long serialVersionUID = 1L;
    
}
