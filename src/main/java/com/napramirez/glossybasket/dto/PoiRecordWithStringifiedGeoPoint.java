package com.napramirez.glossybasket.dto;

public class PoiRecordWithStringifiedGeoPoint {

    private Integer id;

    private String name;

    private String geoPoint;

    public PoiRecordWithStringifiedGeoPoint() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(String geoPoint) {
        this.geoPoint = geoPoint;
    }

}
