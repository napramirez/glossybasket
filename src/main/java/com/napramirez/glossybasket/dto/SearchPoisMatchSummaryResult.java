package com.napramirez.glossybasket.dto;

public class SearchPoisMatchSummaryResult {

    private Integer matchCount;

    private Integer exactMatchCount;

    public SearchPoisMatchSummaryResult() {
    }

    public Integer getMatchCount() {
        return matchCount;
    }

    public void setMatchCount(Integer matchCount) {
        this.matchCount = matchCount;
    }

    public Integer getExactMatchCount() {
        return exactMatchCount;
    }

    public void setExactMatchCount(Integer exactMatchCount) {
        this.exactMatchCount = exactMatchCount;
    }
}
