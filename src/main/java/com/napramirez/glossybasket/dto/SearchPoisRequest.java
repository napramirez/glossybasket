package com.napramirez.glossybasket.dto;

public class SearchPoisRequest {

    private GeoPoint geoPoint;

    public SearchPoisRequest() {
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }
}
