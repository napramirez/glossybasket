package com.napramirez.glossybasket.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchPoisResult {

    @JsonProperty("pois")
    private List<PoiRecordWithLocation> poiRecordsWithLocation;

    public SearchPoisResult() {
    }

    public List<PoiRecordWithLocation> getPoiRecordsWithLocation() {
        return poiRecordsWithLocation;
    }

    public void setPoiRecordsWithLocation(List<PoiRecordWithLocation> poiRecordsWithLocation) {
        this.poiRecordsWithLocation = poiRecordsWithLocation;
    }
}
