package com.napramirez.glossybasket.entity;

import javax.persistence.Embeddable;

@Embeddable
public class GeoPoint {

    private Double latitude;

    private Double longitude;

    public GeoPoint() {
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return String.format("%f,%f", latitude, longitude);
    }

}
