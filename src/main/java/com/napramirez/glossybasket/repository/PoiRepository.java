package com.napramirez.glossybasket.repository;

import java.util.List;

import com.napramirez.glossybasket.entity.Poi;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PoiRepository extends CrudRepository<Poi, Integer> {

	List<Poi> findPoiByLocation(String location);

	Integer countByNameContaining(String string);

	Integer countByName(String string);

	static final String HAVERSINE_PART = "(6371 * acos(cos(radians(:latitude)) * cos(radians(m.geoPoint.latitude)) * cos(radians(m.geoPoint.longitude) - radians(:longitude)) + sin(radians(:latitude)) * sin(radians(m.geoPoint.latitude))))";

	@Query("SELECT m FROM Poi m WHERE " + HAVERSINE_PART + " < :distance ORDER BY " + HAVERSINE_PART + " DESC")
	List<Poi> findNearbyPois(@Param("latitude") Double latitude, @Param("longitude") Double longitude, @Param("distance") Double distance);

}
