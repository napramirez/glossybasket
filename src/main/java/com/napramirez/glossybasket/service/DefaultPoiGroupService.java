package com.napramirez.glossybasket.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.napramirez.glossybasket.dto.PoiRecordWithStringifiedGeoPoint;
import com.napramirez.glossybasket.entity.Poi;
import com.napramirez.glossybasket.repository.PoiRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultPoiGroupService implements PoiGroupService {

    @Autowired
    private PoiRepository poiRepository;

    private ModelMapper modelMapper;

    public DefaultPoiGroupService() {
        modelMapper = new ModelMapper();
    }

    public void setPoiRepository(PoiRepository poiRepository) {
        this.poiRepository = poiRepository;
    }

    @Override
    public List<PoiRecordWithStringifiedGeoPoint> getGroupedPois(String location) {

        if (location == null || location.trim().isEmpty()) {
            return new ArrayList<PoiRecordWithStringifiedGeoPoint>();
        }

        List<Poi> poisInLocation = poiRepository.findPoiByLocation(location);

        return poisInLocation.stream()
            .map(p -> modelMapper.map(p, PoiRecordWithStringifiedGeoPoint.class))
            .collect(Collectors.toList());
    }

}
