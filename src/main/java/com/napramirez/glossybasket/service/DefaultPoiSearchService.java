package com.napramirez.glossybasket.service;

import java.util.List;
import java.util.stream.Collectors;

import com.napramirez.glossybasket.AppConfig;
import com.napramirez.glossybasket.dto.PoiRecordWithLocation;
import com.napramirez.glossybasket.dto.SearchPoisMatchSummaryResult;
import com.napramirez.glossybasket.dto.SearchPoisRequest;
import com.napramirez.glossybasket.dto.SearchPoisResult;
import com.napramirez.glossybasket.entity.Poi;
import com.napramirez.glossybasket.repository.PoiRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultPoiSearchService implements PoiSearchService {

    @Autowired
    private PoiRepository poiRepository;

    @Autowired
    private AppConfig appConfig;

    private ModelMapper modelMapper;

    public DefaultPoiSearchService() {
        modelMapper = new ModelMapper();
    }

    public void setPoiRepository(PoiRepository poiRepository) {
        this.poiRepository = poiRepository;
    }

    public SearchPoisMatchSummaryResult searchByName(String name) {

        SearchPoisMatchSummaryResult result = new SearchPoisMatchSummaryResult();
        result.setMatchCount(poiRepository.countByNameContaining(name));
        result.setExactMatchCount(poiRepository.countByName(name));

        return result;
    }

    @Override
    public SearchPoisResult searchNearbyPois(SearchPoisRequest request) {
        
        List<Poi> nearbyPois = poiRepository.findNearbyPois(
            request.getGeoPoint().getLatitude(),
            request.getGeoPoint().getLongitude(),
            appConfig.getDefaultSearchRadius());

        SearchPoisResult result = new SearchPoisResult();
        result.setPoiRecordsWithLocation(nearbyPois.stream()
            .map(p -> modelMapper.map(p, PoiRecordWithLocation.class))
            .collect(Collectors.toList()));

        return result;
    }

}
