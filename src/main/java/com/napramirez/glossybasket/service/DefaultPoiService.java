package com.napramirez.glossybasket.service;

import java.util.Optional;

import com.napramirez.glossybasket.dto.GeoPoint;
import com.napramirez.glossybasket.dto.UpdatePoiRequest;
import com.napramirez.glossybasket.entity.Poi;
import com.napramirez.glossybasket.repository.PoiRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultPoiService implements PoiService {

    @Autowired
    private PoiRepository poiRepository;

    private ModelMapper modelMapper;
    
    public DefaultPoiService() {
        modelMapper = new ModelMapper();
    }

    public void setPoiRepository(PoiRepository poiRepository) {
        this.poiRepository = poiRepository;
    }

    public Integer updatePoi(Integer id, UpdatePoiRequest request) {
        Optional<Poi> optionalPoi = poiRepository.findById(id);

        if (!optionalPoi.isPresent()) {
            return 0;
        }

        Poi poi = optionalPoi.get();

        String name = request.getName();
        if (name != null && !name.trim().isEmpty()) {
            poi.setName(name);
        }

        String location = request.getLocation();
        if (location != null && !location.trim().isEmpty()) {
            poi.setLocation(location);
        }

        GeoPoint geoPoint = request.getGeoPoint();
        if (geoPoint != null) {
            poi.setGeoPoint(modelMapper.map(geoPoint, com.napramirez.glossybasket.entity.GeoPoint.class));
        }

        poiRepository.save(poi);

        return poi.getId();
    }

}
