package com.napramirez.glossybasket.service;

import java.util.List;

import com.napramirez.glossybasket.dto.PoiRecordWithStringifiedGeoPoint;

public interface PoiGroupService {

	List<PoiRecordWithStringifiedGeoPoint> getGroupedPois(String location);

}
