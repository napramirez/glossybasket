package com.napramirez.glossybasket.service;

import com.napramirez.glossybasket.dto.SearchPoisMatchSummaryResult;
import com.napramirez.glossybasket.dto.SearchPoisRequest;
import com.napramirez.glossybasket.dto.SearchPoisResult;

public interface PoiSearchService {

	SearchPoisResult searchNearbyPois(SearchPoisRequest request);

	SearchPoisMatchSummaryResult searchByName(String name);

}
