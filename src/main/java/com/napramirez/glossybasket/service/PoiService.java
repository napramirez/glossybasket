package com.napramirez.glossybasket.service;

import com.napramirez.glossybasket.dto.UpdatePoiRequest;

public interface PoiService {

	Integer updatePoi(Integer id, UpdatePoiRequest request);

}
