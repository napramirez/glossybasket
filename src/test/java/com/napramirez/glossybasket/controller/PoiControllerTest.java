package com.napramirez.glossybasket.controller;

import static org.junit.jupiter.api.Assertions.fail;

import com.napramirez.glossybasket.service.PoiService;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(PoiController.class)
@AutoConfigureMockMvc
public class PoiControllerTest {
    
    @Autowired
	private MockMvc mockMvc;

    @MockBean
    private PoiService poiService;

	@Test
    void whenPatchPoi_withEmptyRequest_thenExpectHttp400() {
        try {
			mockMvc.perform(MockMvcRequestBuilders.patch("/poi/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
    }

    @Test
    void whenPatchPoi_withEmptyName_thenExpectHttp400() {
        try {
			mockMvc.perform(MockMvcRequestBuilders.patch("/poi/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"name\":\"\"}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
    }

    @Test
    void whenPatchPoi_withEmptyLocation_thenExpectHttp400() {
        try {
			mockMvc.perform(MockMvcRequestBuilders.patch("/poi/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"location\":\"\"}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
    }

    @Test
    void whenPatchPoi_withMissingLatLong_thenExpectHttp400() {
        try {
			mockMvc.perform(MockMvcRequestBuilders.patch("/poi/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
    }

    @Test
    void whenPatchPoi_withMissingLat_thenExpectHttp400() {
        try {
			mockMvc.perform(MockMvcRequestBuilders.patch("/poi/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"long\":0}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
    }

    @Test
    void whenPatchPoi_withLatGreaterThan90_thenExpectHttp400() {
        try {
			mockMvc.perform(MockMvcRequestBuilders.patch("/poi/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"lat\":90.1,\"long\":0}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
    }

    @Test
    void whenPatchPoi_withLatLessThanNeg90_thenExpectHttp400() {
        try {
			mockMvc.perform(MockMvcRequestBuilders.patch("/poi/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"lat\":-90.1,\"long\":0}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
    }

    @Test
    void whenPatchPoi_withMissingLong_thenExpectHttp400() {
        try {
			mockMvc.perform(MockMvcRequestBuilders.patch("/poi/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"lat\":0}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
    }

    @Test
    void whenPatchPoi_withLongGreaterThan180_thenExpectHttp400() {
        try {
			mockMvc.perform(MockMvcRequestBuilders.patch("/poi/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"lat\":0,\"long\":180.1}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
    }

    @Test
    void whenPatchPoi_withLongLessThanNeg180_thenExpectHttp400() {
        try {
			mockMvc.perform(MockMvcRequestBuilders.patch("/poi/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"lat\":0,\"long\":-180.1}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
    }

    @Test
	void whenPatchPoi_withValidRequestBody_thenExpectHttp204() {
		try {
            Mockito.when(poiService.updatePoi(Mockito.any(), Mockito.any())).thenReturn(1);

			mockMvc.perform(MockMvcRequestBuilders.patch("/poi/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"name\":\"Test1\",\"location\":\"21CNR\",\"geoPoint\":{\"lat\":1.359167,\"long\":103.989441}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isNoContent());

            Mockito.verify(poiService, Mockito.times(1)).updatePoi(Mockito.any(), Mockito.any());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

    @Test
    void whenPatchPoi_withValidRequestBody_butOperationFailed_thenExpectHttp500() {
		try {
            Mockito.when(poiService.updatePoi(Mockito.any(), Mockito.any())).thenReturn(0);

			mockMvc.perform(MockMvcRequestBuilders.patch("/poi/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"name\":\"Test1\",\"location\":\"21CNR\",\"geoPoint\":{\"lat\":1.359167,\"long\":103.989441}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isInternalServerError());

            Mockito.verify(poiService, Mockito.times(1)).updatePoi(Mockito.any(), Mockito.any());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
