package com.napramirez.glossybasket.controller;

import static org.junit.jupiter.api.Assertions.fail;

import com.napramirez.glossybasket.service.PoiGroupService;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(PoiGroupController.class)
@AutoConfigureMockMvc
public class PoiGroupControllerTest {
    
    @Autowired
	private MockMvc mockMvc;

    @MockBean
    private PoiGroupService poiGroupService;

    @Test
	void whenGetPoiGroups_thenExpectHttp200_andEmptyResponseBody() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.get("/poi/groups"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.content().string("{}"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	void whenGetPoiGroups_withExistingLocation_thenExpectHttp200_andReturnGroupedPoiInBody() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.get("/poi/groups?location=21CNR"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.21CNR").exists());

            Mockito.verify(poiGroupService, Mockito.times(1)).getGroupedPois("21CNR");
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
