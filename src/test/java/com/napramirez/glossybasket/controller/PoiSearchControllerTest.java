package com.napramirez.glossybasket.controller;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;

import com.napramirez.glossybasket.dto.PoiRecordWithLocation;
import com.napramirez.glossybasket.dto.SearchPoisMatchSummaryResult;
import com.napramirez.glossybasket.dto.SearchPoisResult;
import com.napramirez.glossybasket.service.PoiSearchService;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(PoiSearchController.class)
@AutoConfigureMockMvc
public class PoiSearchControllerTest {

    @Autowired
	private MockMvc mockMvc;

	@MockBean
	private PoiSearchService poiSearchService;

	@Test
	void whenPostPoiSearch_withoutRequestBody_thenExpectHttp400() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/poi/search"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	void whenPostPoiSearch_withEmptyRequestBody_thenExpectHttp400() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/poi/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	void whenPostPoiSearch_withMissingLatLong_thenExpectHttp400() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/poi/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	void whenPostPoiSearch_withMissingLat_thenExpectHttp400() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/poi/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"long\":0}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	void whenPostPoiSearch_withLatGreaterThan90_thenExpectHttp400() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/poi/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"lat\":90.1}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	void whenPostPoiSearch_withLatLessThanNeg90_thenExpectHttp400() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/poi/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"lat\":-90.1}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	void whenPostPoiSearch_withMissingLong_thenExpectHttp400() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/poi/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"lat\":0}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	void whenPostPoiSearch_withLongGreaterThan180_thenExpectHttp400() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/poi/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"long\":180.1}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	void whenPostPoiSearch_withLongLessThanNeg180_thenExpectHttp400() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/poi/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"long\":-180.1}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	void whenPostPoiSearch_withValidRequestBody_thenExpectHttp200_andReturnSearchPoisResult() {
		try {
			SearchPoisResult result = new SearchPoisResult();
			result.setPoiRecordsWithLocation(new ArrayList<PoiRecordWithLocation>());

			Mockito.when(poiSearchService.searchNearbyPois(Mockito.any())).thenReturn(result);

			mockMvc.perform(MockMvcRequestBuilders.post("/poi/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"geoPoint\":{\"lat\":1.359167,\"long\":103.989441}}"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.pois").exists());

			Mockito.verify(poiSearchService, Mockito.times(1)).searchNearbyPois(Mockito.any());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	void whenGetPoiSearch_withoutNameSearchString_thenExpectHttp200_andZeroMatches() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.get("/poi/search"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.matchCount", Matchers.is(0)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.exactMatchCount", Matchers.is(0)));

			Mockito.verify(poiSearchService, Mockito.times(0)).searchByName(Mockito.any());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	void whenGetPoiSearch_withNameSearchString_thenExpectHttp200() {
		try {
			SearchPoisMatchSummaryResult result = new SearchPoisMatchSummaryResult();
            result.setMatchCount(2);
            result.setExactMatchCount(3);

			Mockito.when(poiSearchService.searchByName(Mockito.any())).thenReturn(result);

			mockMvc.perform(MockMvcRequestBuilders.get("/poi/search?name=Test3"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.matchCount").exists())
				.andExpect(MockMvcResultMatchers.jsonPath("$.exactMatchCount").exists());

			Mockito.verify(poiSearchService, Mockito.times(1)).searchByName(Mockito.any());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
