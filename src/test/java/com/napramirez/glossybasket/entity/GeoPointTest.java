package com.napramirez.glossybasket.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GeoPointTest {
    
    @Test
    void whenToString_thenReturnLatLongTupleString() {
        GeoPoint geoPoint = new GeoPoint();
        geoPoint.setLatitude(1.359167);
        geoPoint.setLongitude(103.989441);

        Assertions.assertEquals("1.359167,103.989441", geoPoint.toString());
    }

}
