package com.napramirez.glossybasket.service;

import java.util.Arrays;
import java.util.List;

import com.napramirez.glossybasket.dto.PoiRecordWithStringifiedGeoPoint;
import com.napramirez.glossybasket.entity.GeoPoint;
import com.napramirez.glossybasket.entity.Poi;
import com.napramirez.glossybasket.repository.PoiRepository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class PoiGroupServiceTest {

    private AutoCloseable closeable;

    @Mock
    private PoiRepository poiRepository;

    @InjectMocks
    private DefaultPoiGroupService poiGroupService;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        closeable.close();
    }

    @Test
    void whenGetGroupedPois_withNoSpecifiedLocation_thenReturnNone() {

        List<PoiRecordWithStringifiedGeoPoint> groupedPois = poiGroupService.getGroupedPois(null);

        Mockito.verify(poiRepository, Mockito.times(0)).findPoiByLocation(Mockito.any());

        Assertions.assertNotNull(groupedPois);
        Assertions.assertEquals(0, groupedPois.size());
    }

    @Test
    void whenGetGroupedPois_withSpecificLocation_thenReturnNonEmpty() {

        GeoPoint geoPoint = new GeoPoint();
        geoPoint.setLatitude(1.359167);
        geoPoint.setLongitude(103.989441);

        Poi poi = new Poi();
        poi.setId(1);
        poi.setName("Test1");
        poi.setLocation("21CNR");
        poi.setGeoPoint(geoPoint);

        Mockito.when(poiRepository.findPoiByLocation("21CNR")).thenReturn(Arrays.asList(poi));

        List<PoiRecordWithStringifiedGeoPoint> groupedPois = poiGroupService.getGroupedPois("21CNR");

        Mockito.verify(poiRepository, Mockito.times(1)).findPoiByLocation(Mockito.any());

        Assertions.assertNotNull(groupedPois);
        Assertions.assertNotEquals(0, groupedPois.size());

        PoiRecordWithStringifiedGeoPoint poiRecordWithStringifiedGeoPoint = groupedPois.get(0);
        Assertions.assertNotNull(poiRecordWithStringifiedGeoPoint);
        Assertions.assertEquals(poi.getId(), poiRecordWithStringifiedGeoPoint.getId());
        Assertions.assertEquals(poi.getName(), poiRecordWithStringifiedGeoPoint.getName());
        Assertions.assertEquals(poi.getGeoPoint().toString(), poiRecordWithStringifiedGeoPoint.getGeoPoint());
    }

}
