package com.napramirez.glossybasket.service;

import com.napramirez.glossybasket.AppConfig;
import com.napramirez.glossybasket.dto.GeoPoint;
import com.napramirez.glossybasket.dto.SearchPoisRequest;
import com.napramirez.glossybasket.repository.PoiRepository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class PoiSearchServiceTest {

    private AutoCloseable closeable;

    @Mock
    private PoiRepository poiRepository;

    @Mock
    private AppConfig appConfig;

    @InjectMocks
    private DefaultPoiSearchService poiSearchService;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        closeable.close();
    }

    @Test
    void whenSearchPoiMatchesByName_thenPoiRepositoryShouldBeCalled() {

        poiSearchService.searchByName("Test");

        Mockito.verify(poiRepository, Mockito.times(1)).countByNameContaining("Test");
        Mockito.verify(poiRepository, Mockito.times(1)).countByName("Test");
    }

    @Test
    void whenSearchNearbyPois_thenPoiRepositoryShouldBeCalled() {

        Mockito.when(appConfig.getDefaultSearchRadius()).thenReturn(10.0);

        GeoPoint geoPoint = new GeoPoint();
        geoPoint.setLatitude(1.359167);
        geoPoint.setLongitude(103.989441);

        SearchPoisRequest request = new SearchPoisRequest();
        request.setGeoPoint(geoPoint);

        poiSearchService.searchNearbyPois(request);

        Mockito.verify(poiRepository, Mockito.times(1)).findNearbyPois(Mockito.any(), Mockito.any(), Mockito.any());
    }

}
