package com.napramirez.glossybasket.service;

import java.util.Optional;

import com.napramirez.glossybasket.dto.UpdatePoiRequest;
import com.napramirez.glossybasket.entity.Poi;
import com.napramirez.glossybasket.repository.PoiRepository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class PoiServiceTest {

    private AutoCloseable closeable;

    @Mock
    private PoiRepository poiRepository;

    @InjectMocks
    private DefaultPoiService poiService;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        closeable.close();
    }

    @Test
    void whenUpdatePoi_with_thenPoiRepositoryShouldBeCalled() {

        Poi poi = new Poi();
        poi.setId(1);

        Mockito.when(poiRepository.findById(1)).thenReturn(Optional.of(poi));

        Integer poiId = poiService.updatePoi(1, new UpdatePoiRequest());

        Mockito.verify(poiRepository, Mockito.times(1)).findById(Mockito.any());
        Mockito.verify(poiRepository, Mockito.times(1)).save(Mockito.any());

        Assertions.assertEquals(1, poiId);
    }

}
